# Interview projects

### Todo app:
`git clone -b todo-app https://gitlab.com/vlaats/interview-projects.git`

### Random user app:
`git clone -b random-user-app https://gitlab.com/vlaats/interview-projects.git`

### Photo reviewer app:
`git clone -b photo-reviewer-app https://gitlab.com/vlaats/interview-projects.git`
